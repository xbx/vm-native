package privatecompute

import (
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/protocol/v2"
)

// PrincipalMock mock principal
type PrincipalMock struct {
}

// GetResourceName returns resource name of the verification
func (pm *PrincipalMock) GetResourceName() string {
	panic("implement me")
}

// GetEndorsement returns all endorsements (signatures) of the verification
func (pm *PrincipalMock) GetEndorsement() []*common.EndorsementEntry {
	panic("implement me")
}

// GetMessage returns signing data of the verification
func (pm *PrincipalMock) GetMessage() []byte {
	panic("implement me")
}

// GetTargetOrgId returns target organization id of the verification if the verification is for a specific organization
func (pm *PrincipalMock) GetTargetOrgId() string {
	panic("implement me")
}

// ACProviderMock is ac provider mock
type ACProviderMock struct {
}

// GetHashAlg return hash algorithm the access control provider uses
func (ac *ACProviderMock) GetHashAlg() string {
	panic("implement me")
}

// ValidateResourcePolicy checks whether the given resource policy is valid
func (ac *ACProviderMock) ValidateResourcePolicy(resourcePolicy *config.ResourcePolicy) bool {
	panic("implement me")
}

// LookUpPolicy returns corresponding policy configured for the given resource name
func (ac *ACProviderMock) LookUpPolicy(resourceName string) (*pbac.Policy, error) {
	panic("implement me")
}

// LookUpExceptionalPolicy returns corresponding exceptional policy configured for the given resource name
func (ac *ACProviderMock) LookUpExceptionalPolicy(resourceName string) (*pbac.Policy, error) {
	panic("implement me")
}

// CreatePrincipal creates a principal for one time authentication
func (ac *ACProviderMock) CreatePrincipal(resourceName string, endorsements []*common.EndorsementEntry,
	message []byte) (protocol.Principal, error) {
	return &PrincipalMock{}, nil
}

// CreatePrincipalForTargetOrg creates a principal for "SELF" type policy,
// which needs to convert SELF to a sepecific organization id in one authentication
func (ac *ACProviderMock) CreatePrincipalForTargetOrg(resourceName string, endorsements []*common.EndorsementEntry,
	message []byte, targetOrgId string) (protocol.Principal, error) {
	panic("implement me")
}

//GetValidEndorsements filters all endorsement entries and returns all valid ones
func (ac *ACProviderMock) GetValidEndorsements(principal protocol.Principal) ([]*common.EndorsementEntry, error) {
	panic("implement me")
}

// VerifyPrincipal verifies if the policy for the resource is met
func (ac *ACProviderMock) VerifyPrincipal(principal protocol.Principal) (bool, error) {
	return true, nil
}

// NewMember creates a member from pb Member
func (ac *ACProviderMock) NewMember(member *pbac.Member) (protocol.Member, error) {
	panic("implement me")
}

//GetMemberStatus get the status information of the member
func (ac *ACProviderMock) GetMemberStatus(member *pbac.Member) (pbac.MemberStatus, error) {
	panic("implement me")
}

//VerifyRelatedMaterial verify the member's relevant identity material
func (ac *ACProviderMock) VerifyRelatedMaterial(verifyType pbac.VerifyType, data []byte) (bool, error) {
	panic("implement me")
}

//GetAllPolicy returns all policies
func (ac *ACProviderMock) GetAllPolicy() (map[string]*pbac.Policy, error) {
	panic("implement me")
}
