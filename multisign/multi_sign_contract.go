/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package multisign

import (
	"errors"
	"fmt"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
	"chainmaker.org/chainmaker/vm-native/v2/common"
	"github.com/gogo/protobuf/proto"
)

var (
	contractName = syscontract.SystemContract_MULTI_SIGN.String()
)

// MultiSignContract a multi sign contract
type MultiSignContract struct {
	methods map[string]common.ContractFunc
	log     protocol.Logger
}

// NewMultiSignContract get a multi sign contract
func NewMultiSignContract(log protocol.Logger) *MultiSignContract {
	return &MultiSignContract{
		log:     log,
		methods: InitMultiContractMethods(log),
	}
}

//GetMethod get register method by name
func (c *MultiSignContract) GetMethod(methodName string) common.ContractFunc {
	return c.methods[methodName]
}

// InitMultiContractMethods export method
func InitMultiContractMethods(log protocol.Logger) map[string]common.ContractFunc {
	methodMap := make(map[string]common.ContractFunc, 64)
	runtime := &MultiSignRuntime{log: log}
	methodMap[syscontract.MultiSignFunction_REQ.String()] = common.WrapResultFunc(runtime.Req)
	methodMap[syscontract.MultiSignFunction_VOTE.String()] = common.WrapEventResult(runtime.Vote)
	methodMap[syscontract.MultiSignFunction_QUERY.String()] = common.WrapResultFunc(runtime.Query)
	return methodMap
}

// MultiSignRuntime multi sign method
type MultiSignRuntime struct {
	log protocol.Logger
}

// Req request to multi sign, call a native contract
func (r *MultiSignRuntime) Req(txSimContext protocol.TxSimContext, parameters map[string][]byte) (
	result []byte, err error) {
	// 1、verify param
	sysContractName := parameters[syscontract.MultiReq_SYS_CONTRACT_NAME.String()]
	sysMethod := parameters[syscontract.MultiReq_SYS_METHOD.String()]
	r.log.Infof("multi sign req start. ContractName[%s] Method[%s]", sysContractName, sysMethod)

	if utils.IsAnyBlank(sysContractName, sysMethod) {
		err = fmt.Errorf("multi req params verify fail. sysContractName/sysMethod cannot be empty")
		return nil, err
	}
	if !supportMultiSign(string(sysContractName), string(sysMethod)) {
		err = fmt.Errorf("multi sign not support %s, only support CONTRACT_MANAGE", sysContractName)
		return nil, err
	}
	if err = r.supportRule(txSimContext, sysContractName, sysMethod); err != nil {
		return nil, err
	}

	// building multi sign objects
	tx := txSimContext.GetTx()
	multiSignInfo := &syscontract.MultiSignInfo{
		Payload:      tx.Payload,
		ContractName: string(sysContractName),
		Method:       string(sysMethod),
		Status:       syscontract.MultiSignStatus_PROCESSING,
		VoteInfos:    nil,
	}

	for _, endorser := range tx.Endorsers {
		multiSignInfo.VoteInfos = append(multiSignInfo.VoteInfos, &syscontract.MultiSignVoteInfo{
			Vote:        syscontract.VoteStatus_AGREE,
			Endorsement: endorser,
		})
	}

	// save status
	multiSignInfoBytes, _ := multiSignInfo.Marshal()
	err = txSimContext.Put(contractName, []byte(tx.Payload.TxId), multiSignInfoBytes)
	if err != nil {
		r.log.Warn(err)
		return nil, err
	}

	r.log.Infof("multi sign req end. ContractName[%s] Method[%s], votes count %d",
		sysContractName, sysMethod, len(multiSignInfo.VoteInfos))
	return []byte(tx.Payload.TxId), nil
}

// Vote voting on existing multiSign transaction requests
func (r *MultiSignRuntime) Vote(txSimContext protocol.TxSimContext, parameters map[string][]byte) (
	result []byte, event []*commonPb.ContractEvent, err error) {
	// 1、verify param
	// 2、get history vote record
	// 3、judge vote authority
	// 4、change vote status
	// 5、call actual native contract

	voteInfoBytes := parameters[syscontract.MultiVote_VOTE_INFO.String()]
	txId := parameters[syscontract.MultiVote_TX_ID.String()]
	r.log.Infof("multi sign vote start. MultiVote_TX_ID[%s]", txId)

	if utils.IsAnyBlank(voteInfoBytes, txId) {
		err = fmt.Errorf("multi sign vote params verify fail. voteInfo/txId cannot be empty")
		r.log.Warn(err)
		return nil, nil, err
	}

	multiSignInfoBytes, err := txSimContext.Get(contractName, txId)
	if err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}
	if multiSignInfoBytes == nil {
		return nil, nil, fmt.Errorf("not found tx id[%s]", txId)
	}

	multiSignInfo := &syscontract.MultiSignInfo{}
	err = proto.Unmarshal(multiSignInfoBytes, multiSignInfo)
	if err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}

	// verify: has the user voted
	reqVoteInfo := &syscontract.MultiSignVoteInfo{}
	err = proto.Unmarshal(voteInfoBytes, reqVoteInfo)
	if err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}
	ac, err := txSimContext.GetAccessControl()
	if err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}
	if err = r.hasVoted(ac, reqVoteInfo, multiSignInfo, txId); err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}

	// verify: sign
	mPayloadByte, _ := multiSignInfo.Payload.Marshal()
	resourceName := multiSignInfo.ContractName + "-" + multiSignInfo.Method
	principal, err := ac.CreatePrincipal(resourceName,
		[]*commonPb.EndorsementEntry{reqVoteInfo.Endorsement},
		mPayloadByte)
	if err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}
	endorsement, err := ac.GetValidEndorsements(principal)
	if err != nil {
		r.log.Warn(err)
		return nil, nil, err
	}
	if len(endorsement) == 0 {
		err = fmt.Errorf("the multi sign vote signature[org:%s] is invalid",
			reqVoteInfo.Endorsement.Signer.OrgId)
		r.log.Error(err)
		return nil, nil, err
	}
	multiSignInfo.VoteInfos = append(multiSignInfo.VoteInfos, reqVoteInfo)

	// verify: multi sign
	endorsers := make([]*commonPb.EndorsementEntry, 0)
	for _, info := range multiSignInfo.VoteInfos {
		if info.Vote == syscontract.VoteStatus_AGREE {
			endorsers = append(endorsers, info.Endorsement)
		}
	}
	if len(endorsers) != 0 {
		if ok, err2 := r.verifySignature(ac, resourceName, endorsers, mPayloadByte); err2 != nil {
			return nil, nil, err2
		} else if ok {
			r.log.Infof("multi sign vote [org:%s] verify success, currently %d valid signatures are collected",
				reqVoteInfo.Endorsement.Signer.OrgId, len(endorsers))
			// call contract and set status
			event = r.invokeContract(txSimContext, multiSignInfo)
		}
		//} else {
		// do nothing
		// maybe: authentication fail not enough participants support this action:
		// 3 valid endorsements required, 1 valid endorsements received
	}

	// record status
	multiSignInfoBytes, err = multiSignInfo.Marshal()
	if err != nil {
		r.log.Error(err)
		return nil, nil, err
	}
	err = txSimContext.Put(contractName, txId, multiSignInfoBytes)
	if err != nil {
		r.log.Error(err)
		return nil, nil, err
	}
	r.log.Infof("multi sign vote[%s] end", txId)
	return []byte("OK"), event, nil
}

func (r *MultiSignRuntime) verifySignature(ac protocol.AccessControlProvider, resourceName string,
	endorsers []*commonPb.EndorsementEntry, mPayloadByte []byte) (bool, error) {
	principal, err := ac.CreatePrincipal(resourceName, endorsers, mPayloadByte)
	if err != nil {
		r.log.Warn(err)
		return false, err
	}
	endorsement, err := ac.GetValidEndorsements(principal)
	if err != nil {
		r.log.Warn(err)
		return false, err
	}
	if len(endorsement) == 0 {
		err = fmt.Errorf("multi sign vote error,endorsement:%s is invalid", endorsement)
		r.log.Warn(err)
		return false, err
	}
	multiSignVerify, err := ac.VerifyPrincipal(principal)
	if err != nil {
		r.log.Warn("multi sign vote verify fail.", err)
	}
	return multiSignVerify, nil
}

func (r *MultiSignRuntime) hasVoted(ac protocol.AccessControlProvider,
	reqVoteInfo *syscontract.MultiSignVoteInfo, multiSignInfo *syscontract.MultiSignInfo, txId []byte) error {
	if multiSignInfo.Status != syscontract.MultiSignStatus_PROCESSING {
		err := fmt.Errorf("the multi sign[%s] has been completed", txId)
		r.log.Warn(err)
		return err
	}

	signer, err := ac.NewMember(reqVoteInfo.Endorsement.Signer)
	if err != nil {
		r.log.Warn(err)
		return err
	}
	signerUid := signer.GetUid()
	for _, info := range multiSignInfo.VoteInfos {
		signed, _ := ac.NewMember(info.Endorsement.Signer)
		if signerUid == signed.GetUid() {
			err = fmt.Errorf("the signer[org:%s] is voted", signed.GetUid())
			r.log.Warn(err)
			return err
		}
	}
	return nil
}

func (r *MultiSignRuntime) invokeContract(txSimContext protocol.TxSimContext,
	multiSignInfo *syscontract.MultiSignInfo) []*commonPb.ContractEvent {
	txId := txSimContext.GetTx().Payload.TxId
	contract := &commonPb.Contract{
		Name:        multiSignInfo.ContractName,
		RuntimeType: commonPb.RuntimeType_NATIVE, // multi sign only support native contract
		Status:      commonPb.ContractStatus_NORMAL,
		Creator:     nil,
	}

	initParam := make(map[string][]byte)
	for _, parameter := range multiSignInfo.Payload.Parameters {
		// is sysContractName or sysMethod continue
		if parameter.Key == syscontract.MultiReq_SYS_CONTRACT_NAME.String() ||
			parameter.Key == syscontract.MultiReq_SYS_METHOD.String() {
			continue
		}
		initParam[parameter.Key] = parameter.Value
	}
	byteCode := initParam[syscontract.InitContract_CONTRACT_BYTECODE.String()]
	contractResult, _, statusCode := txSimContext.CallContract(contract, multiSignInfo.Method, byteCode,
		initParam, 0, commonPb.TxType_INVOKE_CONTRACT)
	if statusCode == commonPb.TxStatusCode_SUCCESS {
		multiSignInfo.Message = "OK"
		multiSignInfo.Status = syscontract.MultiSignStatus_ADOPTED
		multiSignInfo.Result = contractResult.Result
		r.log.Infof("multi sign vote[%s] finished, result: %s", txId, contractResult.Result)
		return contractResult.ContractEvent
	}

	contractErr := errors.New(contractResult.Message)
	multiSignInfo.Message = contractErr.Error()
	multiSignInfo.Status = syscontract.MultiSignStatus_FAILED
	r.log.Warnf("multi sign vote[%s] failed, msg: %s", txId, contractErr)
	return nil
}

// Query get multi sign status
func (r *MultiSignRuntime) Query(txSimContext protocol.TxSimContext, parameters map[string][]byte) (
	result []byte, err error) {
	txId := parameters[syscontract.MultiVote_TX_ID.String()]
	if utils.IsAnyBlank(txId) {
		err = fmt.Errorf("multi sign query params verify fail. txId cannot be empty")
		return nil, err
	}

	multiSignInfoDB, err := txSimContext.Get(contractName, txId)
	if err != nil {
		r.log.Error(err)
		return nil, err
	}

	return multiSignInfoDB, nil
}

func supportMultiSign(contractName, method string) bool {
	return contractName == syscontract.SystemContract_CONTRACT_MANAGE.String() ||
		contractName == syscontract.SystemContract_CHAIN_CONFIG.String() ||
		contractName == syscontract.SystemContract_CERT_MANAGE.String() ||
		contractName == syscontract.SystemContract_ACCOUNT_MANAGER.String()
}

func (r *MultiSignRuntime) supportRule(ctx protocol.TxSimContext, name []byte, method []byte) error {
	ac, err := ctx.GetAccessControl()
	if err != nil {
		return err
	}
	resourceName := string(name) + "-" + string(method)
	policy, err2 := ac.LookUpPolicy(resourceName)
	if err2 != nil {
		// not found then there is no authority which means no need to sign multi sign
		r.log.Warn(err2)
		return errors.New("this resource[" + resourceName + "] doesn't support to online multi sign")
	}
	if policy.Rule == string(protocol.RuleSelf) {
		return errors.New("this resource[" + resourceName + "] is the self rule and doesn't support to online multi sign")
	}
	return nil
}
